#!/bin/bash

echo question 1 """Count the number of tweets in the corpora """
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l

echo question 2 """Count the unique number of tweets in the corpora """
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort -u | wc -l

echo question 3 """count the number of retweets in the corpora"""
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep ^'RT'| wc -l

echo question 4 """Print the first unique tweets, not retweets """
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -v ^'RT'| head -20


