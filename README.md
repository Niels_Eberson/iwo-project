Inleiding Wetenschappelijk Onderzoek
====================================

This is a repository for my research. 
I will push here all my files for my research.

Assignment1: Sample-tweets.sh
----------------------------
First I have created a sample-tweets.sh. This is a shell script file that works with twitter data.
The shell script access the inhouse twitter data corpus of the RUG.
It uses the twitter data from 01-03-2017 at 12:00
This shell script give answer to the questions:
1) how many tweets are in the sample?
2) how many unique tweets are in the sample?
3) how many retweets are in the sample (out of the unique tweets)?
4) Show the first 20 unique tweets in the sample that are not retweets. 

------------------------------
